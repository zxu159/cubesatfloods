# Towards Global Flood Extent Mapping On Low Cost Satellites with Machine Learning

This repository contains source code and data used in

> [1] Mateo-Garcia, G., Veitch-Michaelis, J., Smith, L., Oprea, S.,  Schumann, G., Gal, Y., Baydin, A.G., Backes, D., “Towards Global Flood Extent Mapping On Low Cost Satellites with Machine Learning” Scientific Reports 11, 7249 (2021). [open access paper](https://www.nature.com/articles/s41598-021-86650-z)

> :warning: **The code in this repository is deprecated**. Please see **<a href="https://github.com/spaceml-org/ml4floods">ml4floods</a>** for an upgraded version.

![alt text](diagram.png)

## 1. Download the *WorldFloods* database

The *WorldFloods* database contains 444 pairs of Sentinel-2 images and flood segmentation masks. It requires approximately 300GB of hard-disk storage. The file format of images and masks is [GeoTIFF](https://en.wikipedia.org/wiki/GeoTIFF). The dataset is divided in three subsets stored in different folders `train`, `val` and `test`.  The *WorldFloods* database is released under a [Creative Commons non-commercial licence](https://creativecommons.org/licenses/by-nc/4.0/legalcode.txt)

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc.png" alt="licence" width="100"/>

The database is available [here](https://gigatron.uv.es/owncloud/index.php/s/JhNwsrrwDt80Vqc) and in this Google bucket: `gs://ml4cc_data_lake/2_PROD/2_Mart/worldfloods_v1_0/`. This Google bucket is in requester pays mode, hence you'd need a GCP project to download the data. To download the entire dataset run:

```bash
gsutil -u your-project cp gs://ml4cc_data_lake/2_PROD/2_Mart/worldfloods_v1_0/worldfloods_v1_0.zip .
```

If you want only an specific subset (train, train_sample, val or test) run:

```bash
gsutil -u your-project cp gs://ml4cc_data_lake/2_PROD/2_Mart/worldfloods_v1_0/train_v1_0.zip .
gsutil -u your-project cp gs://ml4cc_data_lake/2_PROD/2_Mart/worldfloods_v1_0/train_sample_v1_0.zip .
gsutil -u your-project cp gs://ml4cc_data_lake/2_PROD/2_Mart/worldfloods_v1_0/val_v1_0.zip .
gsutil -u your-project cp gs://ml4cc_data_lake/2_PROD/2_Mart/worldfloods_v1_0/test_v1_0.zip .
```

Checkpoints of the models can be found in the `checkpoints` folder  [here](https://gigatron.uv.es/owncloud/index.php/s/JhNwsrrwDt80Vqc). For other models trained on the *WorldFloods* dataset, checkout the [ml4floods repository](https://github.com/spaceml-org/ml4floods)  which also haver an updated version of the code using `pytorch-lightning`.


## 2. Install in your system

The following code creates a new virtual environment with required dependencies.

```bash
conda create -n cubesatfloods -c conda-forge python=3.7 matplotlib 'pillow<7' numpy scipy libgdal=2.3 pandas \
            h5py albumentations --y
source activate cubesatfloods

# New GPUs (>3.5 compute capability)
conda install pytorch torchvision cudatoolkit=10.1 -c pytorch --y

# OLD GPUS (<=3.5 compute capability)
# conda install pytorch==1.2.0 torchvision==0.4.0 cudatoolkit=10.0 -c pytorch --y

pip install rasterio pytorch-ignite fastprogress termcolor tqdm natsort
```

Run the following to get an overview of options and default values:

```
python europe_floods.py --help
```

## 3. Run

We assume dependencies are installed and data is downloaded in a folder (`--worldfloods_root`) with the following structure:

```
train
├── gt
│   ├── 01042016_Choctawhatchee_River_near_Bellwood_AL.tif
│   ├── 01042016_Holmes_Creek_at_Vernon_FL.tif
....
├── S2
│   ├── 01042016_Choctawhatchee_River_near_Bellwood_AL.tif
│   ├── 01042016_Holmes_Creek_at_Vernon_FL.tif
....
val
├── gt
│   ├── XXX.tif
...
├── S2
│   ├── XXX.tif
....
test
├── gt
│   ├── XXX.tif
...
├── S2
│   ├── XXX.tif
```

### Test

Testing using pre-trained models from `checkpoints` folder:

```
python europe_floods.py --mode test --worldfloods_root WORLDFLOODS --model_folder checkpoints --model linear \
       --test_folder test_linear
```

This will output in the `test_linear` folder the output predictions in `jpeg` format and a `json` file with the confusion matrices for each test image.

Degraded models can be tested using:

```
python europe_floods.py --mode test --worldfloods_root WORLDFLOODS --model_folder checkpoints --model linear_downsampled \
       --test_folder test_linear_downsampled --downsampling_factor 8
```

### Train

Train one of the proposed model in the WorldFloods training dataset (assume training data is downloaded in `WORLDFLOODS/train`):

```
python europe_floods.py --mode train --worldfloods_root WORLDFLOODS --model_folder path_2_save_model --model linear --epochs 1
```

Once the model is trained it can be tested using:

```
python europe_floods.py --mode test --worldfloods_root WORLDFLOODS --model_folder path_2_save_model --model linear \
       --test_folder test_savedmodel
```

### Preprocessing

Groud Truth GeoTiFFs in `gt` subfolder were created from the flood extent maps, [JRC permanent water layer](https://developers.google.com/earth-engine/datasets/catalog/JRC_GSW1_2_YearlyHistory) and the cloud output from the [s2cloudless](https://github.com/sentinel-hub/sentinel2-cloud-detector) model.
This code is included in `preprocessing/build_gtmasks.py`. It requires the data at the `(train|test|val)/floodmaps` and `(train|test|val)/PERMANENTWATERJRC` folders. 

## Acknowledgments

This work is the result of the 2019 ESA [Frontier Development Lab](https://fdleurope.org/) Disaster prevention progress and response team. 
The authors gratefully acknowledge support from the European Space Agency, Google Inc., Intel, Kellogg College, University of Oxford and other organisations and mentors who supported FDL Europe 2019. 

## Cite

If you find this work useful, please cite the [paper](https://www.nature.com/articles/s41598-021-86650-z):

```
@article{mateo-garcia_towards_2021,
	title = {Towards global flood mapping onboard low cost satellites with machine learning},
	volume = {11},
	issn = {2045-2322},
	doi = {10.1038/s41598-021-86650-z},
	number = {1},
	urldate = {2021-04-01},
	journal = {Scientific Reports},
	author = {Mateo-Garcia, Gonzalo and Veitch-Michaelis, Joshua and Smith, Lewis and Oprea, Silviu Vlad and Schumann, Guy and Gal, Yarin and Baydin, Atılım Güneş and Backes, Dietmar},
	month = mar,
	year = {2021},
	pages = {7249},
}
```
